#!/usr/bin/env python

import rospy

from std_msgs.msg import String

import speech_recognition as sr

def speech_text():

    ##Defining topic name 'speech_str' 
    pub = rospy.Publisher('speech_str', String, queue_size=10)
    #Defining the node 'speech_recognition'
    #in ROS, nodes are uniquely named. if two nodes with the same
    #are launched, the previous one is kicked off.
    #anonymous=true flag means that rospy will choose a unique
    rospy.init_node('speech_recognition', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    ##speech recognition function 
    r=sr.Recognizer()

    #Receiving the audio frome microphone
    with sr.Microphone() as source:
        #reads the audio file
	audio = r.listen(source)
        #converting the speech into text by using google api
        speech_str=r.recognize_google(audio)

    #execute the program untill there is an interrupt cntrl-c 
    while not rospy.is_shutdown():
        #message get printed to screen
        rospy.loginfo(speech_str)
        #publishes a string to our 'speech_str' topic
        pub.publish(hello_str)
        rate.sleep()

#start main function
if __name__ == '__main__':
    #calling speech_text() function
    try:
        speech_text()
    except rospy.ROSInterruptException:
        pass
