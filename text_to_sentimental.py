#!/usr/bin/env python

import rospy
from std_msgs.msg import String
#Three different lists that contain the possible words
positive_words=[
             "good morning",
             "fantastic",
             "perfect"
              ]
negative_words=[
             "kill",
             "attack",
             "hell"
              ]

neutral_words=[
            "hello",
            "show",
            "she"
             ]

def callback_and_sentiment_detection(data):

    #rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data.data)
    d=data.data
#Checking the received word is positive,negative or neutral
    for i in range(0,3):

        if d==positive_words[i]:
#if it is positive,then print it
	      rospy.loginfo("it is poitive")

        if d==negative_words[i]:
#if it is negative,then print it
	      rospy.loginfo("it is negative")

        if d==neutral_words[i]:
#if it is neutral,then print it
	      rospy.loginfo("it is neutral")

def text_sentiment():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('sentiment_analysis', anonymous=True)

    rospy.Subscriber('speech_str', String, callback_and_sentiment_detection)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()
#Start main function
if __name__ == '__main__':

    text_sentiment()
